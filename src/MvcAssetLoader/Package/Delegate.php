<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/12/14
 * Time: 11:10 PM
 */

namespace MvcAssetLoader\Package;


use MvcAssetLoader\Loader\AbstractLoader;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Delegate implements ServiceLocatorAwareInterface {

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * Packages to load after the defaults
     * @var array
     */
    protected $afterDefaults = array();

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Loads the default packages
     */
    public function loadDefaults()
    {
        $config = $this->getServiceLocator()->get('config');

        if( isset($config['asset_loader']) && isset($config['asset_loader']['load_packages']))
        {
            foreach($config['asset_loader']['load_packages'] as $package => $enabled)
            {
                if( $enabled )
                {
                    $this->addPackage($package);
                }
            }
        }

        foreach($this->afterDefaults as $package )
        {
            $this->addPackage($package);
        }
    }

    /**
     * @param $package
     * @param $afterDefaults
     * @throws \Exception
     */
    public function add($package, $afterDefaults = false)
    {
        $this->addPackage($package, $afterDefaults);
    }

    /**
     * Adds a package for loading
     * @param $package
     * @param $afterDefaults
     * @throws \Exception
     */
    public function addPackage($package, $afterDefaults = false)
    {
        if( $afterDefaults === true )
        {
            $this->afterDefaults[] = $package;
            return;
        }

        $config = $this->getServiceLocator()->get('config');

        if( ! isset($config['asset_loader']))
        {
            throw new \Exception('Error: No asset loader config set!');
        }

        if( ! isset($config['asset_loader']['packages']))
        {
            throw new \Exception('Error: No asset loader packages config set!');
        }

        $packages = $config['asset_loader']['packages'];

        if( ! isset($packages[$package]) )
        {
            throw new \Exception("No package found by name $package");
        }

        $package = $packages[$package];

        if( isset($package['css']) )
        {
            $css = $this->getServiceLocator()->get('CssAssetLoader');

            foreach($package['css'] as $cssFile => $params)
            {
                if( is_bool($params) && $params === true )
                {
                    $css->add($cssFile, null, AbstractLoader::ADD_PRE_MVC);
                }
                elseif( is_array($params) && isset($params['load']) && $params['load'] == true )
                {
                    $conditions = null;

                    if( isset($params['conditional']))
                    {
                        $conditions = $params['conditional'];
                    }

                    $css->add($cssFile, $conditions, AbstractLoader::ADD_PRE_MVC);
                }
            }
        }

        if( isset($package['javascript']) )
        {
            $java = $this->getServiceLocator()->get('JavascriptAssetLoader');

            foreach($package['javascript'] as $jsFile => $params)
            {
                if( is_bool($params) && $params === true )
                {
                    $java->add($jsFile, null, AbstractLoader::ADD_PRE_MVC);
                }
                elseif( is_array($params) && isset($params['load']) && $params['load'] == true )
                {
                    $conditions = null;

                    if( isset($params['conditional']))
                    {
                        $conditions = $params['conditional'];
                    }

                    $java->add($jsFile, $conditions, AbstractLoader::ADD_PRE_MVC);
                }
            }
        }
    }
} 