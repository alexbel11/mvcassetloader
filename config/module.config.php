<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:04 PM
 */
return array(
    'asset_loader' => array(
        /*'enabled' => true,
        'cache'=>array(
            'enabled'=>true,
            'cache_path'=>'data/cache/assets',
        ),
        'public_path'=>'/public',
        'force_mvc_to_lower'=>false,
        'css'=>array(
            'dir' => '/css/',
            'load_common'   => true,
            'minified'      => true,
            'ie_support'=>array(
                'enabled'=>true,
                'versions'=>array(9,10,11),
            ),
            'libraries'     => array(
                'common' => array(
                    'bootstrap/bootstrap' => array(
                        'load'=>true,
                        'conditional'=>'IE 8',
                        'check_file'=>false,
                    ),
                ),
                'desktop_only' => array(

                ),
                'mobile_only' => array(

                ),
                'guides' => array(

                ),
            ),
        ),
        'javascript'=>array(
            'dir' => '/js/',
            'load_common'   => true,
            'minified'      => true,
            'libraries'     => array(
                'common' => array(
                    'jquery/jquery'         => true,
                    'bootstrap/bootstrap'   => true,
                    'jquery/jquery.mobile-events'=>false,
                ),
                'desktop_only' => array(

                ),
                'mobile_only' => array(

                ),
                'guides' => array(

                ),
            ),
        ),
        'packages'=>array(
            'fancybox'=>array(
                'css'=>array(
                    'fancybox/jquery.fancybox'=>true,
                    'fancybox/helpers/jquery.fancybox-buttons'=>true,
                    'fancybox/helpers/jquery.fancybox-thumbs'=>true,
                ),
                'javascript'=>array(
                    'fancybox/jquery.fancybox'=>true,
                    'fancybox/helpers/jquery.fancybox-buttons'=>true,
                    'fancybox/helpers/jquery.fancybox-media'=>true,
                    'fancybox/helpers/jquery.fancybox-thumbs'=>true,
                ),
            ),
            'validation'=>array(
                'javascript'=>array(

                )
            )
        ),*/
    ),
    'service_manager' => array(
        'invokables' => array(
            'AssetLoaderListener'=>'MvcAssetLoader\Listener\AssetLoaderListener',
            'CssAssetLoader'=>'MvcAssetLoader\Loader\Css',
            'JavascriptAssetLoader'=>'MvcAssetLoader\Loader\Javascript',
            'PackageAssetLoader'=>'MvcAssetLoader\Package\Delegate',
        ),
        'factories'=>array(
            'AssetLoaderCacheAdapter'=>function($sm)
            {
                $config = $sm->get('config'); //cache_dir
                $plugin = \Zend\Cache\StorageFactory::pluginFactory('serializer');
                $cache = \Zend\Cache\StorageFactory::adapterFactory('filesystem', array('cache_dir' => 'data/cache/assets'));
                $cache->addPlugin($plugin);

                return $cache;
            },
        )
    ),
);