<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:46 PM
 */

namespace MvcAssetLoader;
use Zend\Mvc\MvcEvent;

class Module {

    /**
     * Bootstrap Event
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $services = $app->getServiceManager();
        $events = $app->getEventManager();
        $events->attach($services->get('AssetLoaderListener'));
    }

    /**
     * Gets the module config
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Gets the autoloader config.
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
} 